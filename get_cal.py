# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import pytz
import hashlib

def getEventForDay(dom, month, year):
    tmp = ('http://www.iap.fr/actualites/calendrier/calendrier.php' +
           '?date={dom}-{month}-{year}')
    url = tmp.format(dom=dom, month=month, year=year)
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')

    def keep_event_table(tag):
        return tag.find(string='Horaires') and not tag.find('table')

    table = soup.find('table').find(keep_event_table)
    if table is None:
        return

    for row in table.find_all('tr'):
        # Skip headers
        if row.find('th'):
            continue

        rowVal = [cell.contents for cell in row.find_all('td')]
        timeStr = rowVal[0][0].text
        timeStart, timeEnd = timeStr.split('-')
        tz = pytz.timezone('Europe/Paris')
        # TODO: use UTC time
        H, m = [int(e) for e in timeStart.split(':')]
        start = datetime(year, month, dom, H, m, tzinfo=tz)
        H, m = [int(e) for e in timeEnd.split(':')]
        end = datetime(year, month, dom, H, m, tzinfo=tz)

        data = dict(
            start=start,
            end=end,
            kind=rowVal[1][0].text,
            desc=rowVal[1][1].text,
            loc=rowVal[2][0])

        yield data


now = datetime.now()

calStart = '''BEGIN:VCALENDAR
PRODID:-//Mozilla.org/NONSGML Mozilla Calendar V1.1//EN
VERSION:2.0
'''
calStr = ''
calEnd = 'END:VCALENDAR'
for date in (now + timedelta(days=n) for n in range(10)):
    month = date.month
    dom = date.day
    year = date.year
    for event in getEventForDay(dom, month, year):
        h = hash(event['desc']) + \
            hash(event['start']) + \
            hash(event['end'])

        uid = hashlib.md5(('%s' % h).encode('utf-8')).hexdigest()

        # Creating calendar entry
        timeFmt = '%Y%m%dT%H%M%SZ'
        calStr += u'BEGIN:VEVENT\n'
        calStr += u'UID:%s\n' % uid
        calStr += u'DTSTART:%s\n' % event['start'].strftime(timeFmt)
        calStr += u'DTEND:%s\n' % event['end'].strftime(timeFmt)
        calStr += u'DTSTAMP:%s\n' % datetime.now().strftime(timeFmt)
        calStr += u'SUMMARY:%s\n' % event['kind']
        calStr += u'DESCRIPTION:%s\n' % event['desc']
        calStr += u'LOCATION:%s\n' % event['loc']
        calStr += u'END:VEVENT\n'

calStr = calStart + calStr + calEnd
calStr = calStr.replace('\n', '\r\n')
print(calStr.encode('utf-8'))
