#!/usr/bin/bash

FNAME=iap_calendar.ics
DEST=/some/path/to/put/the/ics/file
source venv/bin/activate
python get_cal.py > $FNAME

cp $FNAME $DEST
